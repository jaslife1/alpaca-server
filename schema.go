package main

import (
	"errors"
	"log"

	graphql "github.com/graphql-go/graphql"
	"github.com/jaslife1/uniuri"
	pb_advertisement "gitlab.com/lazybasterds/advertisement-service/proto/advertisement"
	pb_building "gitlab.com/lazybasterds/building-service/proto/building"
	pb_map "gitlab.com/lazybasterds/map-service/proto/map"
	pb_node "gitlab.com/lazybasterds/node-service/proto/node"
	pb_search "gitlab.com/lazybasterds/search-service/proto/search"
)

var schema, _ = graphql.NewSchema(graphql.SchemaConfig{
	Query: rootQuery,
	//Mutation: mutationQuery,
})

var buildingType = graphql.NewObject(graphql.ObjectConfig{
	Name: "BuildingInfo",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"location": &graphql.Field{
			Type: graphql.String,
		},
		"description": &graphql.Field{
			Type: graphql.String,
		},
		"image": &graphql.Field{
			Type: graphql.NewList(imageType),
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				building, ok := params.Source.(*(pb_building.Building))
				if ok {
					var images []interface{}
					var err error
					for _, id := range building.Image {
						reqID := uniuri.New()
						tmp, err := getImage(reqID, id)
						if err != nil {
							log.Printf("FAIL: Resolve - building info image, RequestID: %s, Message: Failed to retrieve image with id: %s", reqID, id)
							continue
						}
						images = append(images, tmp)
					}
					return images, err
				}

				return nil, errors.New("FAIL: Resolve - building info image, Message: Failed to retrieve type of source")
			},
		},
	},
})

var floorType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Floor",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var imageType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Image",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"length": &graphql.Field{
			Type: graphql.Float,
		},
		"chunksize": &graphql.Field{
			Type: graphql.Float,
		},
		"uploaddate": &graphql.Field{
			Type: graphql.String,
		},
		"filename": &graphql.Field{
			Type: graphql.String,
		},
		"data": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var nodeType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Node",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"type": &graphql.Field{
			Type: graphql.String,
		},
		"coordinate": &graphql.Field{
			Type: coordinateType,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {

				switch node := params.Source.(type) {
				case *(pb_node.Node):
					return node.Coordinate, nil
				case *(pb_search.Node):
					return node.Coordinate, nil
				case *(pb_map.Node):
					return node.Coordinate, nil
				default:
					return nil, errors.New("FAIL: Resolve - node coordinate, Message: Failed to retrieve type of source")
				}
			},
		},
		"floor": &graphql.Field{
			Type: floorType,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {

				switch node := params.Source.(type) {
				case *(pb_node.Node):
					return getFloor(node.FloorID)
				case *(pb_search.Node):
					return getFloor(node.FloorID)
				case *(pb_map.Node):
					return getFloor(node.FloorID)
				default:
					return nil, errors.New("FAIL: Resolve - node floor, Message: Failed to retrieve type of source")
				}
			},
		},
		"details": &graphql.Field{
			Type: nodeDetailsType,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {

				switch node := params.Source.(type) {
				case *(pb_node.Node):
					return node.Details, nil
				case *(pb_search.Node):
					return node.Details, nil
				case *(pb_map.Node):
					return node.Details, nil
				default:
					return nil, errors.New("FAIL: Resolve - node details, Message: Failed to retrieve type of source")
				}
			},
		},
	},
})

var coordinateType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Coordinate",
	Fields: graphql.Fields{
		"x": &graphql.Field{
			Type: graphql.String,
		},
		"y": &graphql.Field{
			Type: graphql.String,
		},
		"z": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var nodeDetailsType = graphql.NewObject(graphql.ObjectConfig{
	Name: "NodeDetails",
	Fields: graphql.Fields{
		"description": &graphql.Field{
			Type: graphql.String,
		},
		"logoid": &graphql.Field{
			Type: graphql.ID,
		},
		"imageid": &graphql.Field{
			Type: graphql.ID,
		},
		"location": &graphql.Field{
			Type: graphql.String,
		},
		"category": &graphql.Field{
			Type: graphql.String,
		},
		"tags": &graphql.Field{
			Type: graphql.NewList(graphql.String),
		},
		"openinghours": &graphql.Field{
			Type: openingHoursType,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				switch details := params.Source.(type) {
				case *(pb_node.Details):
					return details.OpeningHours, nil
				case *(pb_search.Details):
					return details.OpeningHours, nil
				case *(pb_map.Details):
					return details.OpeningHours, nil
				default:
					return nil, errors.New("FAIL: Resolve - node details opening hours, Message: Failed to retrieve type of source")
				}
			},
		},
		"logo": &graphql.Field{
			Type: imageType,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				reqID := uniuri.New()
				switch details := params.Source.(type) {
				case *(pb_node.Details):
					logoID := details.Logoid
					return getImage(reqID, logoID)
				case *(pb_search.Details):
					logoID := details.Logoid
					return getImage(reqID, logoID)
				case *(pb_map.Details):
					logoID := details.Logoid
					return getImage(reqID, logoID)
				default:
					return nil, errors.New("FAIL: Resolve - node details logo, Message: Failed to retrieve type of source")
				}
			},
		},
		"image": &graphql.Field{
			Type: imageType,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				reqID := uniuri.New()
				switch details := params.Source.(type) {
				case *(pb_node.Details):
					imageID := details.Imageid
					return getImage(reqID, imageID)
				case *(pb_search.Details):
					imageID := details.Imageid
					return getImage(reqID, imageID)
				case *(pb_map.Details):
					imageID := details.Imageid
					return getImage(reqID, imageID)
				default:
					return nil, errors.New("FAIL: Resolve - node details image , Message: Failed to retrieve type of source")
				}
			},
		},
	},
})

var openingHoursType = graphql.NewObject(graphql.ObjectConfig{
	Name: "OpeningHours",
	Fields: graphql.Fields{
		"sunday": &graphql.Field{
			Type: graphql.String,
		},
		"monday": &graphql.Field{
			Type: graphql.String,
		},
		"tuesday": &graphql.Field{
			Type: graphql.String,
		},
		"wednesday": &graphql.Field{
			Type: graphql.String,
		},
		"thursday": &graphql.Field{
			Type: graphql.String,
		},
		"friday": &graphql.Field{
			Type: graphql.String,
		},
		"saturday": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var timeType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Time",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"weeknumber": &graphql.Field{
			Type: graphql.Int,
		},
		"utcoffset": &graphql.Field{
			Type: graphql.String,
		},
		"unixtime": &graphql.Field{
			Type: graphql.String,
		},
		"timezone": &graphql.Field{
			Type: graphql.String,
		},
		"dstuntil": &graphql.Field{
			Type: graphql.String,
		},
		"dstfrom": &graphql.Field{
			Type: graphql.String,
		},
		"dst": &graphql.Field{
			Type: graphql.Boolean,
		},
		"dayofyear": &graphql.Field{
			Type: graphql.Int,
		},
		"dayofweek": &graphql.Field{
			Type: graphql.Int,
		},
		"datetime": &graphql.Field{
			Type: graphql.String,
		},
		"abbreviation": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var weatherType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Weather",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"coord": &graphql.Field{
			Type: coordType,
		},
		"weather": &graphql.Field{
			Type: graphql.NewList(mainWeatherType),
		},
		"base": &graphql.Field{
			Type: graphql.String,
		},
		"main": &graphql.Field{
			Type: mainType,
		},
		"visibility": &graphql.Field{
			Type: graphql.Float,
		},
		"wind": &graphql.Field{
			Type: windType,
		},
		"clouds": &graphql.Field{
			Type: cloudsType,
		},
		"datetime": &graphql.Field{
			Type: graphql.Float,
		},
		"system": &graphql.Field{
			Type: sysType,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var coordType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Coord",
	Fields: graphql.Fields{
		"latitude": &graphql.Field{
			Type: graphql.Float,
		},
		"longitude": &graphql.Field{
			Type: graphql.Float,
		},
	},
})

var mainWeatherType = graphql.NewObject(graphql.ObjectConfig{
	Name: "MainWeather",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"main": &graphql.Field{
			Type: graphql.String,
		},
		"description": &graphql.Field{
			Type: graphql.String,
		},
		"icon": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var mainType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Main",
	Fields: graphql.Fields{
		"tempink": &graphql.Field{
			Type: graphql.Float,
		},
		"tempinc": &graphql.Field{
			Type: graphql.Float,
		},
		"tempinf": &graphql.Field{
			Type: graphql.Float,
		},
		"pressure": &graphql.Field{
			Type: graphql.Float,
		},
		"humidity": &graphql.Field{
			Type: graphql.Float,
		},
		"tempmin": &graphql.Field{
			Type: graphql.Float,
		},
		"tempmax": &graphql.Field{
			Type: graphql.Float,
		},
	},
})

var windType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Wind",
	Fields: graphql.Fields{
		"speed": &graphql.Field{
			Type: graphql.Float,
		},
		"degree": &graphql.Field{
			Type: graphql.Float,
		},
	},
})

var cloudsType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Clouds",
	Fields: graphql.Fields{
		"all": &graphql.Field{
			Type: graphql.Int,
		},
	},
})

var sysType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Sys",
	Fields: graphql.Fields{
		"type": &graphql.Field{
			Type: graphql.Int,
		},
		"id": &graphql.Field{
			Type: graphql.Int,
		},
		"message": &graphql.Field{
			Type: graphql.Float,
		},
		"country": &graphql.Field{
			Type: graphql.String,
		},
		"sunrise": &graphql.Field{
			Type: graphql.Float,
		},
		"sunset": &graphql.Field{
			Type: graphql.Float,
		},
	},
})

var advertisementType = graphql.NewObject(graphql.ObjectConfig{
	Name: "Advertisement",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"nodeid": &graphql.Field{
			Type: graphql.ID,
		},
		"title": &graphql.Field{
			Type: graphql.String,
		},
		"description": &graphql.Field{
			Type: graphql.String,
		},
		"image": &graphql.Field{
			Type: graphql.NewList(imageType),
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				advertisement, ok := params.Source.(*(pb_advertisement.Advertisement))
				if ok {
					var images []interface{}
					var err error
					for _, id := range advertisement.Image {
						reqID := uniuri.New()
						tmp, err := getImage(reqID, id)
						if err != nil {
							log.Printf("FAIL: Resolve - advertisement image, RequestID: %s, Message: Failed to retrieve image with id: %s", reqID, id)
							continue
						}
						images = append(images, tmp)
					}
					return images, err
				}

				return nil, errors.New("FAIL: Resolve - advertisement image, Message: Failed to retrieve type of source")
			},
		},
	},
})
