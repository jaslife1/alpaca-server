package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	graphql "github.com/graphql-go/graphql"
	micro "github.com/micro/go-micro"
	_ "github.com/micro/go-plugins/registry/kubernetes"
	k8s "github.com/micro/kubernetes/go/micro"
	pb_advertisement "gitlab.com/lazybasterds/advertisement-service/proto/advertisement"
	pb_building "gitlab.com/lazybasterds/building-service/proto/building"
	pb_image "gitlab.com/lazybasterds/image-service/proto/image"
	pb_map "gitlab.com/lazybasterds/map-service/proto/map"
	pb_node "gitlab.com/lazybasterds/node-service/proto/node"
	pb_search "gitlab.com/lazybasterds/search-service/proto/search"
	pb_time "gitlab.com/lazybasterds/time-service/proto/time"
	pb_weather "gitlab.com/lazybasterds/weather-service/proto/weather"
)

type payload struct {
	Query         string                 `json:"query"`
	Variables     map[string]interface{} `json:"variables,omitempty"`
	operationName string
}

const (
	defaultStartLocation = "5c7498e31c9d44000049e4a0"
)

var (
	buildingServiceClient      pb_building.BuildingServiceClient
	imageServiceClient         pb_image.ImageServiceClient
	nodeServiceClient          pb_node.NodeServiceClient
	searchServiceClient        pb_search.SearchServiceClient
	timeServiceClient          pb_time.TimeServiceClient
	weatherServiceClient       pb_weather.WeatherServiceClient
	advertisementServiceClient pb_advertisement.AdvertisementServiceClient
	mapServiceClient           pb_map.MapServiceClient
)

func serveAPIRequest(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers")

	log.Printf("Request: %+v\n", r)

	if r.URL.Path != "/api/v1/" {
		http.Error(w, "404 Page not found.", http.StatusNotFound)
		return
	}

	switch r.Method {
	case "OPTIONS":
		log.Println("Received options...Expecting post with payload")
	case "POST":

		tmp := make([]byte, r.ContentLength)
		read, err := r.Body.Read(tmp) //for success, err =EOF
		defer r.Body.Close()

		if int64(read) != r.ContentLength {
			log.Println("Read mismatch with ContentLength. Read: ", read, ", ContentLength: ", r.ContentLength)
			http.Error(w, "Read mismatch. Internal server error.", http.StatusInternalServerError)
		}

		if err != io.EOF {
			log.Println("Reading Body did not end.")
		}

		var load payload
		err = json.Unmarshal(tmp, &load)

		log.Printf("Payload: %+v", load)

		if err != nil {
			log.Println("Error when unmarshalling query. Err: ", err)
		}

		result := graphql.Do(graphql.Params{
			Schema:         schema,
			RequestString:  load.Query,
			VariableValues: load.Variables,
			Context:        r.Context(),
		})

		json.NewEncoder(w).Encode(result)
	default:
		http.Error(w, "Only POST and OPTIONS method is supported.", http.StatusBadRequest)
	}

}

func main() {
	start := time.Now()
	log.Println("Running Alpaca server...")

	srv := k8s.NewService(
		micro.Name("alpaca-server"),
		micro.Version("latest"),
	)

	// Init will parse the command line flags.
	srv.Init()

	initServiceClients(srv)

	elapsed := time.Since(start)
	log.Printf("Total Execution time for setting up server: %s\n", elapsed)

	http.HandleFunc("/api/v1/", serveAPIRequest)

	go func() {
		log.Println("Listening for connections at port 4000...")
		http.ListenAndServe(":4000", nil)
	}()

	quit := make(chan os.Signal)
	signal.Notify(quit, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	log.Println("Closing Alpaca server...")
	elapsed = time.Since(start)
	log.Printf("Total server uptime: %s\n", elapsed)
	log.Println("Alpaca server shutdown.")
}

func initServiceClients(srv micro.Service) {
	buildingServiceClient = pb_building.NewBuildingServiceClient("building-service", srv.Client())
	imageServiceClient = pb_image.NewImageServiceClient("image-service", srv.Client())
	nodeServiceClient = pb_node.NewNodeServiceClient("node-service", srv.Client())
	searchServiceClient = pb_search.NewSearchServiceClient("search-service", srv.Client())
	timeServiceClient = pb_time.NewTimeServiceClient("time-service", srv.Client())
	weatherServiceClient = pb_weather.NewWeatherServiceClient("weather-service", srv.Client())
	advertisementServiceClient = pb_advertisement.NewAdvertisementServiceClient("advertisement-service", srv.Client())
	mapServiceClient = pb_map.NewMapServiceClient("map-service", srv.Client())
}
