package main

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/graphql-go/graphql"
	"github.com/jaslife1/uniuri"
	"github.com/micro/go-micro/metadata"
	pb_advertisement "gitlab.com/lazybasterds/advertisement-service/proto/advertisement"
	pb_building "gitlab.com/lazybasterds/building-service/proto/building"
	pb_map "gitlab.com/lazybasterds/map-service/proto/map"
	pb_node "gitlab.com/lazybasterds/node-service/proto/node"
	pb_search "gitlab.com/lazybasterds/search-service/proto/search"
	pb_time "gitlab.com/lazybasterds/time-service/proto/time"
	pb_weather "gitlab.com/lazybasterds/weather-service/proto/weather"
)

//GraphQL Root Query node
var rootQuery = graphql.NewObject(graphql.ObjectConfig{
	Name: "Query",
	Fields: graphql.Fields{
		"getBuildingInfo": &graphql.Field{
			Type: buildingType,
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				// Call to the grpc
				reqID := uniuri.New()
				log.Println("Query - getBuildingInfo, RequestID: ", reqID)
				ctx := metadata.NewContext(context.Background(), map[string]string{
					"requestID": reqID,
				})
				duration := time.Now().Add(defaultDurationLength)
				ctx, cancel := context.WithDeadline(ctx, duration)
				defer cancel()
				resp, err := buildingServiceClient.GetBuildingInfo(ctx, &pb_building.Building{})

				if err != nil {
					log.Printf("FAIL: Query - getBuildingInfo, RequestID: %s, Message: Did not get any building information\n", reqID)
					log.Println(err)
					return nil, err
				}
				log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
				return resp.Building, nil
			},
		},
		"floors": &graphql.Field{
			Type: graphql.NewList(floorType),
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				// Call to the grpc
				reqID := uniuri.New()
				log.Println("Query - floors, RequestID: ", reqID)
				ctx := metadata.NewContext(context.Background(), map[string]string{
					"requestID": reqID,
				})
				duration := time.Now().Add(defaultDurationLength)
				ctx, cancel := context.WithDeadline(ctx, duration)
				defer cancel()
				resp, err := buildingServiceClient.GetAllFloors(ctx, &pb_building.Floor{})

				if err != nil {
					log.Printf("FAIL: Query - floors, RequestID: %s, Message: Did not get all floors\n", reqID)
					log.Println(err)
					return nil, err
				}
				log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
				return resp.Floors, nil
			},
		},
		"getImage": &graphql.Field{
			Type: imageType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.ID),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				id, ok := params.Args["id"].(string)
				reqID := uniuri.New()
				if ok {
					return getImage(reqID, id)
				}
				return nil, errors.New("FAIL: Query - getImage, RequestID: " + reqID + ", Message: Did not get image with id " + id)
			},
		},
		"stores": &graphql.Field{
			Type: graphql.NewList(nodeType),
			Args: graphql.FieldConfigArgument{
				"floorid": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"sort": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				floorID, _ := params.Args["floorid"].(string)
				sortBy, _ := params.Args["sort"].(string)

				reqID := uniuri.New()
				log.Println("Query - stores, RequestID: ", reqID)
				return getNodes(reqID, floorID, "Store", sortBy)
			},
		},

		"getStore": &graphql.Field{
			Type: nodeType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				nodeID, ok := params.Args["id"].(string)

				reqID := uniuri.New()
				if ok {
					log.Println("Query - getStore, RequestID: ", reqID)
					return getNodeByID(reqID, nodeID)
				}
				return nil, errors.New("FAIL: Query - getStore, RequestID: " + reqID + ", Message: Did not get store with ID " + nodeID)
			},
		},

		"facilities": &graphql.Field{
			Type: graphql.NewList(nodeType),
			Args: graphql.FieldConfigArgument{
				"floorid": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"type": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
				"sort": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				floorID, _ := params.Args["floorid"].(string)
				facilityType, _ := params.Args["type"].(string)
				sortBy, _ := params.Args["sort"].(string)

				reqID := uniuri.New()
				log.Println("Query - facilities, RequestID: ", reqID)
				return getNodes(reqID, floorID, facilityType, sortBy)
			},
		},

		"getFacility": &graphql.Field{
			Type: nodeType,
			Args: graphql.FieldConfigArgument{
				"id": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				nodeID, ok := params.Args["id"].(string)

				reqID := uniuri.New()
				if ok {
					log.Println("Query - getFacility, RequestID: ", reqID)
					return getNodeByID(reqID, nodeID)
				}
				return nil, errors.New("FAIL: Query - getFacility, RequestID: " + reqID + ", Message: Did not get facility with ID " + nodeID)
			},
		},

		"search": &graphql.Field{
			Type: graphql.NewList(nodeType),
			Args: graphql.FieldConfigArgument{
				"query": &graphql.ArgumentConfig{
					Type: graphql.String,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				query, ok := params.Args["query"].(string)
				reqID := uniuri.New()
				if ok {
					// Call to the grpc
					log.Println("Query - search, RequestID: ", reqID)
					ctx := metadata.NewContext(context.Background(), map[string]string{
						"requestID": reqID,
					})
					duration := time.Now().Add(defaultDurationLength)
					ctx, cancel := context.WithDeadline(ctx, duration)
					defer cancel()
					resp, err := searchServiceClient.Search(ctx, &pb_search.Query{Query: query})

					if err != nil {
						log.Printf("FAIL: Query - search, RequestID: %s, Message: Did not find any nodes with %s\n", reqID, query)
						log.Println(err)
						return nil, err
					}
					log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
					return resp.Nodes, nil
				}
				return nil, errors.New("FAIL: Query - search, RequestID: " + reqID + "Message: Failed to search for query %s" + query)
			},
		},
		"getCurrentTime": &graphql.Field{
			Type: timeType,
			Args: graphql.FieldConfigArgument{
				"timezone": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				timezone, ok := params.Args["timezone"].(string)
				reqID := uniuri.New()
				if ok {
					// Call to the grpc
					log.Println("Query - getCurrentTime , RequestID: ", reqID)
					ctx := metadata.NewContext(context.Background(), map[string]string{
						"requestID": reqID,
					})
					duration := time.Now().Add(defaultDurationLength)
					ctx, cancel := context.WithDeadline(ctx, duration)
					defer cancel()
					resp, err := timeServiceClient.GetCurrentTime(ctx, &pb_time.Time{Timezone: timezone})

					if err != nil {
						log.Printf("FAIL: Query - getCurrentTime, RequestID: %s, Message: Did not get time for timezone %s\n", reqID, timezone)
						log.Println(err)
						return nil, err
					}
					log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
					return resp.Time, nil
				}

				return nil, errors.New("FAIL: Query - getCurrentTime, RequestID: " + reqID + "Message: Failed to get time for timezone %s" + timezone)
			},
		},
		"getWeather": &graphql.Field{
			Type: weatherType,
			Args: graphql.FieldConfigArgument{
				"locale": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.String),
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				locale, ok := params.Args["locale"].(string)
				reqID := uniuri.New()
				if ok {
					// Call to the grpc
					log.Println("Query - getWeather , RequestID: ", reqID)
					ctx := metadata.NewContext(context.Background(), map[string]string{
						"requestID": reqID,
					})
					duration := time.Now().Add(defaultDurationLength)
					ctx, cancel := context.WithDeadline(ctx, duration)
					defer cancel()
					resp, err := weatherServiceClient.GetWeather(ctx, &pb_weather.Locale{Locale: locale})

					if err != nil {
						log.Printf("FAIL: Query - getWeather, RequestID: %s, Message: Did not get weather for locale %s\n", reqID, locale)
						log.Println(err)
						return nil, err
					}
					log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
					return resp.Weather, nil
				}

				return nil, errors.New("FAIL: Query - getWeather, RequestID: " + reqID + "Message: Failed to get weather for locale %s" + locale)
			},
		},
		"getActivePromotions": &graphql.Field{
			Type: graphql.NewList(advertisementType),
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				// Call to the grpc
				reqID := uniuri.New()
				log.Println("Query - getActivePromotions, RequestID: ", reqID)
				ctx := metadata.NewContext(context.Background(), map[string]string{
					"requestID": reqID,
				})
				duration := time.Now().Add(defaultDurationLength)
				ctx, cancel := context.WithDeadline(ctx, duration)
				defer cancel()
				resp, err := advertisementServiceClient.GetActiveAdvertisements(ctx, &pb_advertisement.Advertisement{})

				if err != nil {
					log.Printf("FAIL: Query - getActivePromotions, RequestID: %s, Message: Did not get any active promotions\n", reqID)
					log.Println(err)
					return nil, err
				}
				log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
				return resp.Advertisements, nil
			},
		},
		"getPath": &graphql.Field{
			Type: graphql.NewList(nodeType),
			Args: graphql.FieldConfigArgument{
				"destination": &graphql.ArgumentConfig{
					Type: graphql.NewNonNull(graphql.ID),
				},
				"currentLocation": &graphql.ArgumentConfig{
					Type: graphql.ID,
				},
			},
			Resolve: func(params graphql.ResolveParams) (interface{}, error) {
				dest, ok1 := params.Args["destination"].(string)
				start, ok2 := params.Args["currentLocation"].(string)
				if !ok2 {
					start = defaultStartLocation
				}
				reqID := uniuri.New()
				if ok1 {
					// Call to the grpc
					log.Println("Query - getPath , RequestID: ", reqID)
					ctx := metadata.NewContext(context.Background(), map[string]string{
						"requestID": reqID,
					})
					duration := time.Now().Add(defaultDurationLength)
					ctx, cancel := context.WithDeadline(ctx, duration)
					defer cancel()

					resp, err := mapServiceClient.GetPath(ctx, &pb_map.Path{Start: &pb_map.Node{Id: start}, Goal: &pb_map.Node{Id: dest}})

					if err != nil {
						log.Printf("FAIL: Query - getPath, RequestID: %s, Message: Did not get path from %s to %s\n", reqID, start, dest)
						log.Println(err)
						return nil, err
					}
					log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
					return resp.Nodes, nil
				}
				return nil, errors.New("FAIL: Query - getPath, RequestID: " + reqID + "Message: Failed to get path from " + start + " to " + dest)
			},
		},
	},
})

func getNodes(reqID, floorID, nodeType, sort string) (interface{}, error) {
	// Call to the grpc
	ctx := metadata.NewContext(context.Background(), map[string]string{
		"requestID": reqID,
	})
	duration := time.Now().Add(defaultDurationLength)
	ctx, cancel := context.WithDeadline(ctx, duration)
	defer cancel()
	var resp *pb_node.NodeResponse
	var err error
	if floorID != "" {
		resp, err = nodeServiceClient.GetNodesPerFloor(ctx, &pb_node.Node{FloorID: floorID, Type: nodeType, SortBy: sort})
	} else {
		resp, err = nodeServiceClient.GetAllNodes(ctx, &pb_node.Node{Type: nodeType, SortBy: sort})
	}

	if err != nil {
		log.Printf("FAIL: Query - getNodes, RequestID: %s, Message: Did not get getNodes in floor with ID %s and type %s\n", reqID, floorID, nodeType)
		log.Println(err)
		return nil, err
	}
	log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
	return resp.Nodes, nil
}

func getNodeByID(reqID, nodeID string) (interface{}, error) {
	// Call to the grpc
	ctx := metadata.NewContext(context.Background(), map[string]string{
		"requestID": reqID,
	})
	duration := time.Now().Add(defaultDurationLength)
	ctx, cancel := context.WithDeadline(ctx, duration)
	defer cancel()
	resp, err := nodeServiceClient.GetNodeByID(ctx, &pb_node.Node{Id: nodeID})

	if err != nil {
		log.Printf("FAIL: Query - getNodeByID, RequestID: %s, Message: Did not get Node with ID %s\n", reqID, nodeID)
		log.Println(err)
		return nil, err
	}
	log.Printf("Response: RequestID: %s, Result: %+v", reqID, resp)
	return resp.Node, nil
}
